<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;


class Category extends Model
{
    protected $fillable = ['name', 'description', 'image'];

    public static $rules = [
        'name' => 'required|min:3',
    ];

    protected static function boot(){
        parent::boot();
        static::deleting(function(Category $category){
            if(substr($category->image, 0, 4) !== 'http'){
                $fullPath = public_path().'/images/categories/'.$category->image;
                File::delete($fullPath);
            }
        });
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
    public function getUrlImageAttribute(){

        if($this->image == NULL){
            return '/images/categories/default.png';
        }
        else{
            if (substr($this->image, 0, 4) === "http") {
                return $this->image;
            }
        }
        return asset('/images/categories/'.$this->image);
    }

}
