<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;
use Intervention\Image\ImageManager;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::orderBy('created_at','DESC')->get();
        return view('admin.categories.index')->with(compact('categories'));
    }
    public function create(){
        return view('admin.categories.create');
    }
    public function store(Request $request){

        //dd($request->all()); //muestra todos los datos request
        $success = true;
        $this->validate($request, Category::$rules);

        DB::beginTransaction();
        try{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $path = public_path().'/images/categories/temp';
                $fileName = uniqid().$file->getClientOriginalName();
                if($file->move($path, $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $img = $intervention->make(public_path().'/images/categories/temp/'.$fileName)->fit(524, 150);
                    $img->save(public_path().'/images/categories/'.$fileName);
                    File::delete(public_path().'/images/categories/temp/'.$fileName);
                }
            }
            $category = Category::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'image' => $fileName
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'categoria registrada correctamente']);
            return redirect('admin/categories');
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }
    }
    public function edit(Category $category){
        return view('admin.categories.edit')->with(compact('category')); //formulario de edicion de categorias
    }
    public function update(Request $request, Category $category){

        $success = true;
        $this->validate($request, Category::$rules);

        DB::beginTransaction();
        try{
            if($request->hasFile('image')){

                $file = $request->file('image');
                $path = public_path().'/images/categories/temp';
                File::delete(public_path().'/images/categories/'.$category->image);

                $fileName = uniqid().$file->getClientOriginalName();
                Category::find($category->id)->update([
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'image' => $fileName
                ]);
                if($file->move($path, $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $img = $intervention->make(public_path().'/images/categories/temp/'.$fileName)->fit(524, 150);
                    $img->save(public_path().'/images/categories/'.$fileName);
                    File::delete(public_path().'/images/categories/temp/'.$fileName);
                }
            }
            else{
                $category->update([
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                ]);
            }
        }catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){

            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'categoria actualizada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }

    public function delete(Category $category){
        $success = true;
        DB::beginTransaction();
        try{
            $category->delete();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'categoria eliminada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }



}
