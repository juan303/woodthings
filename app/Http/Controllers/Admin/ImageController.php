<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;
use Intervention\Image\ImageManager;

class ImageController extends Controller
{
    public function index($id){
        $product = Product::find($id);
        $images = $product->product_images()->orderBy('featured', 'DESC')->get();
        return view('admin.products.images.index')->with(compact('product', 'images'));
    }
    public function store(Request $request, $id){
        $success = true;
        $file = $request->file('image');
        $path = public_path().'/images/products/temp';
        $fileName = uniqid().$file->getClientOriginalName();
        $thumb_fileName = 'thumb_'.$fileName;
        if($file->move($path, $fileName)){
            $intervention = new ImageManager(['driver'=>'gd']);
            $image_thumb = $intervention->make(public_path().'/images/products/temp/'.$fileName)->fit(400, 300);
            $image = $intervention->make(public_path().'/images/products/temp/'.$fileName)->fit(800, 600);

            if($image_thumb->save(public_path().'/images/products/thumbnails/'.$thumb_fileName) &&
                $image->save(public_path().'/images/products/'.$fileName)){
                File::delete(public_path().'/images/products/temp/'.$fileName);
            }

            DB::beginTransaction();
            try{
                $product_image = Image::create([
                    'image' => $fileName,
                    'thumb_image' => $thumb_fileName,
                    'product_id' => $id
                ]);
            }catch(\Exception $exception){
                $success = $exception->getMessage();
                DB::rollBack();
            }
            if($success === true){
                DB::commit();
                if(count(Product::find($id)->product_images) == 1){
                    $product_image->destacar($id);
                }
                session()->flash('message', ['type' => 'success', 'text'=>'imagen registrada correctamente']);
            }
            else{
                session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            }
        }
        return back()->with(compact('notification', 'error'));
    }
    public function delete(Image $image){

        $image->delete();
        if ($image->featured == true) {
            $product = $image->product;
            $image = $product->product_images->first();
            if ($image) {
                $image->destacar($product->id);
            }
        }

        return back();
    }

    public function destacar_imagen($product_id, $image_id){

        $product_image = Image::find($image_id);
        $product_image->destacar($product_id);

        return back();
    }

}
