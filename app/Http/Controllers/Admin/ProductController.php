<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;


class ProductController extends Controller
{
    public function index(){
        $products = Product::orderBy('created_at', 'desc')->paginate(12);
        return view('admin.products.index')->with(compact('products'));
    }
    public function create(){
        $categories = Category::all();
        return view('admin.products.create')->with(compact('categories'));
    }
    public function store(Request $request){
        //dd($request->input('description'));
        $this->validate($request, Product::$rules);

        $success = true;
        DB::beginTransaction();
        try{
            $product = Product::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'long_description' => $request->input('long_description'),
                'price' => $request->input('price'),
                'category_id' => $request->input('category'),
                'user_id' => auth()->user()->id
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'articulo registrado correctamente']);
            return redirect('admin/products/images/'.$product->id);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(Product $product){
        $images = $product->images;
        $categories = Category::all();
        return view('admin.products.edit')->with(compact('product', 'images', 'categories')); //formulario de registro
    }

    public function update(Product $product, Request $request){
        //dd($request);
        $this->validate($request, Product::$rules, Product::$messages);
        $success = true;

        DB::beginTransaction();
        try{
            $product->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'long_description' => $request->input('long_description'),
                'price' => $request->input('price'),
                'category_id' => $request->input('category'),
            ]);
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'cambios guardados correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }

    public function delete(Product $product){
        $success = true;
        $product_images = $product->product_images;
        foreach($product_images as $product_image){
            $product_image->delete();
        }
        DB::beginTransaction();
        try{
            $product->delete();
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            return response()->json(['type' => 'success', 'text' => 'Producto #'.$product->id.' eliminado correctamente']);
            //session()->flash('message', ['type' => 'success', 'text'=>'Producto eliminado correctamente']);
        }
        else{
            return response()->json(['type' => 'warning', 'text' => $success]);
            //session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        //return back();
    }



}
