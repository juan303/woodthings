<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::orderBy('created_at','DESC')->get()->paginate(15);
        return view('admin.users.index')->with(compact('users'));
    }
}
