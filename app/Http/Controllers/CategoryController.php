<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('categories.index')->with(compact('categories'));
    }

    public function show(Category $category){
        $products = $category->products()->paginate(6);
        $categories = Category::all();
        return view('categories.show')->with(compact('products', 'category', 'categories'));
    }
}
