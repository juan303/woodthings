<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessage;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function index(){
        return view('contact');
    }
    public function index_product(Product $product){
        return view('contact')->with(compact('product'));
    }
    public function send(Request $request){
        if(Mail::to('gdf000@hotmail.com')->send(new ContactMessage($request))){
            session()->flash('message', ['type' => 'success', 'text'=>'Mensaje enviado. En breve recibirá una respuesta']);
        }
        else{
            session()->flash('message', ['type' => 'success', 'text'=>'Mensaje enviado. En breve recibirá una respuesta']);
        }
        return back();
    }
}
