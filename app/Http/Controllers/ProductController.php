<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class ProductController extends Controller
{
    public function products_category(Category $category){
        $products = $category->products;
        return view('products.index')->with(compact('products'));
    }
    public function show(Product $product){
        $images = $product->product_images;
        $intervention = new ImageManager(['driver'=>'imagick']);
        return view('products.show')->with(compact('product','images', 'intervention'));
    }
}
