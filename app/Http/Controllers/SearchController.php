<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
        $text = $request->input('text');
        $products = Product::where('name', 'like', '%'.$text.'%')->paginate(1);
        return view('search')->with(compact('products', 'text'));
    }
}
