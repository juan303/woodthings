<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class WelcomeController extends Controller
{
    public function index(){
        $last_products = Product::take(6)->orderBy('created_at','DESC')->get();
        return view('welcome')->with(compact('last_products'));
    }
}
