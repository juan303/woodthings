<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use File;

class Image extends Model
{
    protected $fillable  = ['product_id','image', 'thumb_image'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    protected static function boot(){
        parent::boot();
        static::deleting(function(Image $image){
            if(substr($image->image, 0, 4) !== 'http'){
                $fullPath = public_path().'/images/products/'.$image->image;
                $fullPath_thumb = public_path().'/images/products/thumbnails/'.$image->thumb_image;
                File::delete($fullPath);
                File::delete($fullPath_thumb);
            }
        });

    }

    public function destacar($product_id){
        Image::where('product_id', $product_id)->update([
            'featured' => false,
        ]);
        $this->featured = true;
        $this->save();
    }

    public function getUrlAttribute(){
        if(substr($this->image, 0, 4) === "http"){
            return $this->image;
        }
        return asset('/images/products/'.$this->image);
    }

    public function getThumbnailUrlAttribute(){
        if(substr($this->image, 0, 4) === "http"){
            return $this->image;
        }
        return asset('/images/products/thumbnails/'.$this->thumb_image);
    }


}
