<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['name', 'description', 'long_description', 'price', 'user_id', 'category_id'];

    public static $messages = [
        'name.required' => 'El campo nombre es obligatorio',
        'name.min' => 'El campo nombre debe contener 3 caracteres como mínimo',
        'description.required' => 'El campo descripcion es obligatorio',
        'price.required' => 'El campo del precio es obligatorio',
        'price.numeric' => 'El campo del precio debe ser numérico',
        'price.min' => 'El campo del precio debe ser un numero positivo',
    ];

    public static $rules = [
        'name' => 'required|min:3',
        'description' => 'required',
        'price' => 'required|numeric|min:0'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function product_images(){
        return $this->hasMany(Image::class);
    }

    //ACCESSOR
    public function getFeaturedImageUrlAttribute(){
        $featuredImage = $this->product_images()->where('featured', true)->first();
        if(!$featuredImage){
            $featuredImage = $this->product_images()->first();
            if(!$featuredImage){
                return 'images/products/default.png';
            }
        }
        return $featuredImage->url;
    }
    public function getCategoryNameAttribute(){

        if(!isset($this->category)){
            return "General";
        }
        return $this->category->name;
    }
}
