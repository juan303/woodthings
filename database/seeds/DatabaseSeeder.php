<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Category;
use App\Image;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create([
            'name' => 'bob303',
            'email' => 'gdf000@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        /*factory(Category::class, 5)->create()
            ->each(function(Category $c){
                factory(Product::class, 7)->create(['category_id' => $c->id])
                    ->each(function(Product $p){
                       factory(Image::class, 4)->create(['product_id'=> $p->id]);
                    });
            });*/
    }
}
