@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('content')
    <div class="section section-team">
        <div class="container">
            @include('partials.messages.general_messages')
            <h2 class="title">{{ __('Editar categoria').": ".$category->name }}</h2>
            <div class="team">
                <form class="form" method="post" action="{{ url('/admin/categories/update/'.$category->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="name" class="mr-auto">{{ __('Nombre') }}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $category->name) }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description" class="mr-auto">{{ __('Descripción') }}</label>
                                <input value="{{ old('description', $category->description) }}" type="text" class="form-control" id="description" name="description">
                                @if ($errors->has('description'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <img src="{{ $category->url_image }}" alt="">
                            <div class="form-group">
                                <label for="description" class="mr-auto">{{ __('Imagen') }}</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="image">Buscar imagen</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" href="{{ route('list_categories') }}" class="btn btn-warning btn-round btn-lg" type="submit">{{ __('Guardar') }}</button>
                        <a href="{{ route('list_categories') }}" class="btn btn-warning btn-round btn-lg">Volver</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection