@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('content')
    <div class="section section-team text-center">
        <div class="container">
            @include('partials.messages.general_messages')
            <h2 class="title">{{ __('Categorias') }}</h2>
            <div class="team">
                <div class="row">
                    <a href="{{ route('create_category') }}" class="btn btn-success">
                        {{ __("Registrar nueva categoria") }}
                    </a>
                    <table class="table col-md-12">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Imagen</th>
                            <th class="text-left">Nombre</th>
                            <th class="d-none d-sm-table-cell text-left">Descripcion</th>
                            <th class="text-right" style="width:15%">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td class="text-center">{{ $category->id }}</td>
                                <td class="text-center"><img src="{{ $category->url_image }}" style="height: 60px; min-width: 144px" alt=""></td>
                                <td class="text-left">{{ $category->name }}</td>
                                <td class="text-left d-none d-sm-table-cell">{{ $category->description }}</td>
                                <td class="td-actions text-right">
                                   {{-- <a href="{{ url('products/'.$product->id) }}" target="_blank" rel="tooltip" title="Detalles" class="btn btn-link px-1 text-info my-0 py-0">
                                        <i class="fa fa-info"></i>
                                    </a> --}}
                                    <a href="{{ url('admin/categories/edit/'.$category->id) }}"  rel="tooltip" title="Editar datos" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{ url('admin/categories/'.$category->id) }}" method="post" class="d-inline">
                                    {{ csrf_field() }} <!-- es equivalente a <input type="hidden" name="_token" value="csrf_token" /> -->
                                    {{ method_field('DELETE') }} <!-- es equivalente a <input type="hidden" name="_method" value="DELETE" /> -->
                                        <button type="submit" rel="tooltip" title="Eliminar categoria" class="btn px-1 btn-link text-danger my-0 py-0">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection