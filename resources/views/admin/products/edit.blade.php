@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('content')

    <div class="section section-team">
        <div class="container">
            @include('partials.messages.general_messages')
            <h2 class="title">{{ __('Editar producto').": ".$product->name }}</h2>
            <div class="team">
                <form class="form" method="post" action="{{ url('/admin/products/update/'.$product->id) }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="name" class="mr-auto">{{ __('Nombre') }}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $product->name) }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description" class="mr-auto">{{ __('Descripción corta') }}</label>
                                <input value="{{ old('description', $product->description) }}" type="text" class="form-control" id="description" name="description">
                                @if ($errors->has('description'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="long_description" class="mr-auto">{{ __('Descripción') }}</label>
                                <textarea class="form-control" id="long_description" name="long_description">{{ old('long_description', $product->long_description) }}</textarea>
                                @if ($errors->has('long_description'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('long_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="name" class="mr-auto">{{ __('Nombre') }}</label>
                                <select name="category" id="category" class="custom-select">
                                    @foreach($categories as $category)
                                        <option @if(isset($product->category) && $product->category->id === $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price" class="mr-auto">{{ __('Precio') }}</label>
                                <input value="{{ old('price', $product->price) }}" type="number" step="0.01" class="form-control" id="price" name="price">
                                @if ($errors->has('price'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="{{ route('list_products') }}" class="btn btn-warning btn-round btn-lg" type="submit">{{ __('Volver') }}</a>
                        <button class="btn btn-primary btn-round btn-lg" type="submit">{{ __('Guardar cambios') }}</button>
                        <a href="{{ url('admin/products/images/'.$product->id) }}" class="btn btn-warning btn-round btn-lg" type="submit">{{ __('Editar imagenes') }}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection