@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('content')
    <div class="section section-team text-center">
        <div class="container">
            <h2 class="title">{{ __('Subir imagenes para: ')." ".$product->name }}</h2>
            <div class="team">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="now-ui-icons ui-2_like"></i>
                            </div>
                            {{ session('message')['text'] }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <form action="{{ url('admin/products/images/'.$product->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="image">Imagen</label>
                                        <input class="custom-file-input" type="file" id="image" name="image">
                                    </div>
                                    <button type="submit" class="btn btn-success">{{ __('Subir') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @foreach($images as $image)
                    <div class="col-lg-3 col-md-6">
                            <div class="card  @if($image->featured == true) border-primary border @endif">
                                <img class="card-img-top" src="{{ $image->url }}" alt="Card image cap">
                                <div class="card-body">
                                    <form action="{{ url('admin/products/images/'.$image->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button href="#" class="btn btn-danger">{{ __('Eliminar') }}</button>
                                        @if($image->featured != true)
                                            <a href="{{ url('admin/products/images/select/'.$product->id.'/'.$image->id) }}" class="btn btn-success btn-just-icon" rel="tooltip" title="Destacar imagen">
                                                <i class="now-ui-icons ui-2_favourite-28"></i>
                                            </a>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-12">
                        <a href="{{ url('admin/products') }}" class="btn btn-warning">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection