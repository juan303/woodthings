@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.btn-delete').click(function (e) {
                e.preventDefault();
                var form = $(this).parents('form');
                var row = $(this).parents('tr');
                var url = form.attr('action');
                $.post(url, form.serialize(), function(data){
                    row.fadeOut();
                    $('#mensaje').addClass('alert-'+data.type);
                    $('#mensaje').html(data.text);
                }).fail(function(){
                    $('#mensaje').addClass('alert-warning');
                    $('#mensaje').html('no se ha podido eliminar el registro');
                })
                $('#mensaje').fadeIn();
            })
        })
    </script>
@endsection

@section('content')
    <div class="section section-team text-center">
        <div class="container">
            @include('partials.messages.general_messages')
            <div class="alert" style="display: none;" id="mensaje"></div>
            <h2 class="title">{{ __('Productos') }}</h2>
            <div class="team">
                <div class="row">
                    <a href="{{ route('create_product') }}" class="btn btn-success">
                        {{ __("Registrar producto") }}
                    </a>
                    <table class="table col-md-12">
                        <thead>
                        <tr>
                            <th class="text-center d-none d-sm-table-cell">#</th>
                            <th class="">Nombre</th>
                            <th>Categoría</th>
                            <th class="text-right">Precio</th>
                            <th class="text-right" style="width:15%">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td class="text-center d-none d-sm-table-cell">{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>

                                <td>{{ $product->category_name }}</td>
                                <td class="text-right">{{ $product->price }}€</td>
                                <td class="td-actions text-right">
                                    <a href="{{ url('products/'.$product->id) }}" target="_blank" rel="tooltip" title="Detalles" class="btn btn-link px-1 text-info my-0 py-0">
                                        <i class="fa fa-info"></i>
                                    </a>
                                    <a href="{{ url('admin/products/edit/'.$product->id) }}"  rel="tooltip" title="Editar datos" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('admin/products/images/'.$product->id) }}" rel="tooltip" title="Editar imagenes" class="btn btn-link px-1 text-warning px-0 my-0 py-0">
                                        <i class="fa fa-image"></i>
                                    </a>
                                    <form action="{{ url('admin/products/'.$product->id) }}" method="post" class="d-inline">
                                    {{ csrf_field() }} <!-- es equivalente a <input type="hidden" name="_token" value="csrf_token" /> -->
                                    {{ method_field('DELETE') }} <!-- es equivalente a <input type="hidden" name="_method" value="DELETE" /> -->
                                        <button type="submit" rel="tooltip" title="Eliminar producto" class="btn btn-delete px-1 btn-link text-danger my-0 py-0">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection