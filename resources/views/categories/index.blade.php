@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top d-block')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection
@section('styles')

@endsection

<!-- End Navbar -->
@section('content')

    @include('partials.categories.category_nav')

    <div class="section section-team text-center bg-transparent" style=" padding-top:100px;">
        <div class="container">
            <h2 class="title">{{ __('Categorias') }}</h2>
            <div class="team">
                <div class="row">
                    @foreach ($categories as $category)
                        @include('partials.categories.category_card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
