@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection


<!-- End Navbar -->
@section('content')

    @include('partials.categories.category_nav')

    <div class="section section-team text-center" style="background-color:  #feecd3;  padding-top:100px;">
        <div class="container">
            <h2 class="title">{{ $category->name }}</h2>
            <div class="team">
                <div class="row">
                    @foreach ($products as $product)
                        @include('partials.products.product_card')
                    @endforeach
                </div>
                {{ $products->links() }}
            </div>
        </div>
        <a class="btn btn-warning" href="{{ route('categories') }}">Volver</a>
    </div>

@endsection