@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top d-block')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection
@section('styles')
    <style>
        .img-container {
            height: 200px;
            max-height: 200px;
            overflow: hidden;
            position: relative;
        }
        .img-container > .crop {
            position:absolute;
            left: -100%;
            right: -100%;
            top: -100%;
            bottom: -100%;
            margin: auto;
            min-height: 100%;
            min-width: 100%;
            transition:all .5s ease-in-out;
        }
        .img-container:hover > .crop{
            -webkit-transform:scale(1.3);
            transform: scale(1.3);
            transition:all .5s ease-in-out;
        }

    </style>
@endsection

<!-- End Navbar -->
@section('content')

    <div class="section section-team text-center bg-transparent" style=" padding-top:100px;">
        <div class="container">
            <h2 class="title">{{ __('Contáctanos') }}</h2>
            <div class="team">
                @include('partials.messages.general_messages')
                <div class="row">
                        <div class="card p-3 bg-light">
                            <h4 class="card-title">
                                Formulario de contacto
                            </h4>
                            <form action="{{ route('contact.send') }}" method="post">
                                @csrf
                            <div class="card-body row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block text-left pl-2" for="exampleFormControlInput1">{{ __('Nombre') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block text-left pl-2" for="exampleFormControlInput1">{{ __('Correo electrónico') }}</label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block text-left pl-2" for="exampleFormControlInput1">{{ __('Asunto') }}</label>
                                        <input type="text" class="form-control" id="subject" name="subject" value="{{ old('subject', 'Sobre "'.$product->name.'"') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block text-left pl-2" for="exampleFormControlInput1">{{ __('Mensaje') }}</label>
                                        <textarea rows="7" type="text" class="form-control" id="mtext" name="text" style="max-height:180px">{{ old('text') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary" type="submit">{{ __('Enviar') }}</button>
                                </div>
                            </div>
                            </form>
                        </div>

                </div>
            </div>
        </div>
    </div>

@endsection
