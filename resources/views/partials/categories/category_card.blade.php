<div class="col-md-12 col-lg-6">
    <div class="card p-2">
        <div class="team-player">
            <div class="img-container img-container-category">
                <a href="{{ url('categories/'.$category->id) }}"><img class="crop img-fluid" src="{{ $category->url_image }}" alt="{{ $category->image }}"></a>
            </div>
            <h4 class="title pt-0"><a href="{{ url('categories/'.$category->id) }}">{{ $category->name }}</a></h4>
            <p class="description">{{ $category->description }}</p>
        </div>
    </div>
</div>