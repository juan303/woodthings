<nav class="nav navbar-expand bg-dark text-light fixed-top" style="top:60px; z-index:1">
    <div class="container">
        <h3 class="btn btn-primary btn-outline-primary d-none d-md-inline-flex">
            Categorias:
        </h3>
        <button class="btn btn-primary w-100 d-sm-block d-md-none" type="button" data-toggle="collapse" data-target="#categories" aria-expanded="false" aria-controls="categories">
            Categorias:
        </button>
        <div class="d-xs-none d-md-inline-flex collapse">
            <ul class="navbar-nav">
                @foreach($categories as $cat)
                    <li class="nav-item">
                        @if(isset($category) && $category->id == $cat->id)
                            <strong>
                                <a class="nav-link text-light" href="{{ url('categories/'.$cat->id) }}">{{ $cat->name }}</a>
                            </strong>
                        @else
                            <a class="nav-link" href="{{ url('categories/'.$cat->id) }}">{{ $cat->name }}</a>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="collapse text-center" id="categories">
            @foreach($categories as $cat)
                @if(isset($category) && $category->id == $cat->id)
                    <strong>
                        <a class="nav-link d-block d-md-none" href="{{ url('categories/'.$cat->id) }}">{{ $cat->name }}</a>
                    </strong>
                @else
                    <a class="nav-link" href="{{ url('categories/'.$cat->id) }}">{{ $cat->name }}</a>
                @endif
            @endforeach
        </div>
    </div>
</nav>