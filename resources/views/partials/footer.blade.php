<footer class="footer footer-default">
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="">
                        Sobre mi
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright" id="copyright">
            &copy;
            <script>
                document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, {{ __('Diseñado por ') }} <a href="">Juan Esteban</a>
        </div>
    </div>
</footer>