
@if(session('message'))
    <div class="alert alert-success" role="alert">
        <div class="container">
            <div class="alert-icon">
                @if(session('message')['type'] == 'danger')
                    <i class="far fa-thumbs-down"></i>
                @else
                    <i class="far fa-thumbs-up"></i>
                @endif
            </div>
            {{ session('message')['text'] }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </span>
            </button>
        </div>
    </div>
@endif