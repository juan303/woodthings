<nav class="@yield('nav-class')" @yield('transparency')>
    <div class="container sidebar-collapse">
        <div class="navbar-translate">
            <a class="navbar-brand" href="{{ route('welcome') }}"  data-placement="bottom">
                WoodThings
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar"></span>
                <span class="navbar-toggler-bar middle-bar"></span>
                <span class="navbar-toggler-bar bottom-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end navigation" id="navigation1" data-nav-image="{{ asset('img/blurred-image-1.jpg') }}">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('categories') }}">Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">Consúltanos</a>
                </li>

                @if(auth()->user())

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ auth()->user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" v-bind:aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('list_categories') }}">{{ __('Administrar categorías') }}</a>
                            <a class="dropdown-item" href="{{ route('list_products') }}">{{ __('Administrar productos') }}</a>

                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Cerrar sesión') }}

                            </a>
                            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>


                @endif

               {{-- <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="{{ __('Siguenos en Twitter') }}" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                        <i class="fab fa-twitter"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="{{ __('Siguenos en Facebook') }}" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="{{ __('Siguenos en Instagram') }}" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                        <i class="fab fa-instagram"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="{{ __('Siguenos en Pinterest') }}" data-placement="bottom" href="https://www.pinterest.es/woodthings2018/" target="_blank">
                        <i class="fab fa-pinterest"></i>
                        <p class="d-lg-none d-xl-none">Pinterest</p>
                    </a>
                </li>
            </ul>
        </div>



    </div>

</nav>








