<div class="col-sm-6 col-lg-4">
    <div class="card p-2" style="transform: rotate({{ mt_rand(-1.5*10,1.5*10)/10 }}deg)">
        <div class="team-player">
            <div class="img-container img-container-product">
                <a href="{{ url('/products/'.$product->id) }}"><img class="crop img-fluid" src="{{ $product->featured_image_url }}" alt="Card image cap"></a>
            </div>
            {{--<img src="{{ $product->featured_image_url }}" alt="Thumbnail Image" class="img-fluid img-raised">--}}
            <h4 class="title pt-0"><a class="text-primary" href="{{ url('/products/'.$product->id) }}">{{ $product->name }}</a></h4>
            @if(!mb_stristr(Request::path(),'categories'))
                <p class="category text-capitalize">@if(!isset($product->category)) General @else <a class="text-warning" href="{{ url('categories/'.$product->category->id) }}">{{$product->category->name}}</a> @endif</p>
            @endif
            <p class="description">{{ Str::words($product->description, 11) }}</p>
            {{--<a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
            <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-instagram"></i></a>
            <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-facebook-square"></i></a>--}}
        </div>
    </div>
</div>