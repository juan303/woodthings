@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection
@section('styles')
    <style>
        .img-container {
            height: 200px;
            max-height: 200px;
            overflow: hidden;
            position: relative;
        }
        .img-container > .crop {
            position:absolute;
            left: -100%;
            right: -100%;
            top: -100%;
            bottom: -100%;
            margin: auto;
            min-height: 100%;
            min-width: 100%;
            transition:all .5s ease-in-out;
        }
        .img-container:hover > .crop{
            -webkit-transform:scale(1.3);
            transform: scale(1.3);
            transition:all .5s ease-in-out;
        }

    </style>
@endsection

<!-- End Navbar -->
@section('content')
    {{--<div class="page-header page-header-ultra-small">
        <div class="page-header-image" data-parallax="true" style="background-image: url('{{ asset('images/portada.jpg') }}');">
        </div>
        <div class="content-center">
            <div class="container">
                <h1 class="title" style="font-size:5vh">Decoración con madera</h1>
                <div class="text-center">
                    <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-google-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>--}}

    <div class="section section-team text-center" style="background-color:  #feecd3">
        <div class="container">
            <h2 class="title">{{ __('Categorias') }}</h2>
            <div class="team">
                <div class="row">
                    @foreach ($categories as $category)
                        @include('partials.categories.category_card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection