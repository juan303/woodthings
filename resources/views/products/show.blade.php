@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');
    </script>
    <script type="text/javascript" src="{{ asset('js/lightgallery-all.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#lightgallery").lightGallery({
                getCaptionFromTitleOrAlt: false,
                download: false,
            });
            $('.img-gallery').mouseover(function () {
                $('#main-img').attr('src',$(this).attr('data-src'))
            })
        })

    </script>
@endsection
@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset('css/lightgallery.css') }}" />
@endsection

<!-- End Navbar -->
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 p-3 rounded shadow" style="margin-bottom:100px; margin-top: 90px; background-color:  #feecd3;">
                <h2 class="title text-center">{{ $product->name }}</h2>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="col-12 mb-4 px-0">
                            <img id="main-img" class="img-fluid rounded w-100" src="{{ $product->featured_image_url }}" alt="">
                        </div>
                        <div class="col-12">
                            <div class="row" id="lightgallery">
                                @foreach ($images as $image)
                                    <div class="col-3 px-0 img-gallery" data-src="{{ $image->url }}" data-responsive="{{ $image->url }} 757">
                                        <a  href="">
                                            <img class="img-thumbnail rounded" src="{{ $image->thumbnail_url }}" alt="{{ 'image_'.$product->name.'_'.$image->id }}">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 text-left mt-3">
                        <p>{{ $product->long_description }}</p>
                        <p><a class="font-weight-bold text-warning" href="{{ url('contact/'.$product->id) }}">{{ __('Consulta sobre este proyecto') }}</a></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <a href="{{ back()->getTargetUrl() }}" class="btn btn-warning">{{ __('Volver') }}</a>
                </div>
            </div>
        </div>
    </div>

@endsection