@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top d-block')
@section('transparency', '')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection


<!-- End Navbar -->
@section('content')

    <div class="section section-team text-center bg-transparent">
        <div class="container mt-4">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <form action="">
                        @csrf
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text pr-3" id="inputGroupPrepend"><i class="fas fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control bg-light" id="validationCustomUsername" placeholder="{{ __('Buscar...') }}" aria-describedby="inputGroupPrepend" required>
                            <div class="input-group-append">
                                <span class="input-group-text pl-3" id="inputGroupPrepend"></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <h2 class="title"> {{ __('Buscar').": ".$text }}</h2>
            <div class="team">
                <div class="row">
                    @foreach ($products as $product)
                        @include('partials.products.product_card')
                    @endforeach
                </div>
                <div class="row">
                    {{ $products->appends(['text'=>$text])->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection