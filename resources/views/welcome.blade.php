@extends('layouts.app')

@section('nav-class', 'navbar navbar-expand-lg bg-primary fixed-top navbar-transparent d-block')
@section('transparency', 'color-on-scroll=250')

@section('scripts')
    <script type="text/javascript">
        var scroll = new SmoothScroll('a[href*="#_"]');


    </script>
@endsection


<!-- End Navbar -->
@section('content')
    <div class="page-header page-header-ultra-small">
        <div class="page-header-image" data-parallax="true" style="background-image: url('{{ asset('images/portada.JPG') }}');">
        </div>
        <div class="content-center">
            <div class="container">
                <h1 class="title" style="font-size:5vh">{{ __('Decoración en madera') }}</h1>
                <div class="text-center">
                   {{-- <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                        <i class="fab fa-google-plus"></i>
                    </a> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="section section-team text-center bg-transparent">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <form action="{{ url('search') }}">
                        @csrf
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text pr-3" id="inputGroupPrepend"><i class="fas fa-search"></i></span>
                            </div>
                            <input type="text" name="text" class="form-control bg-light" id="validationCustomUsername" placeholder="{{ __('Buscar...') }}" aria-describedby="inputGroupPrepend" required>
                            <div class="input-group-append">
                                <span class="input-group-text pl-3" id="inputGroupPrepend"></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <h2 class="title">{{ __('Novedades') }}</h2>
            <div class="team">
                <div class="row">
                    @foreach ($last_products as $product)
                        @include('partials.products.product_card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection