<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::middleware(['auth'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
});

//AUTENTICACION
Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');

//PRODUCTOS
Route::get('products/{product}', 'ProductController@show');


//CATEGORIAS
Route::get('categories', 'CategoryController@index')->name('categories');
Route::get('categories/{category}', 'CategoryController@show');


//CONTACTO
Route::get('contact', 'MessageController@index')->name('contact');
Route::get('contact/{product}', 'MessageController@index_product');
Route::post('contact', 'MessageController@send')->name('contact.send');

//BUSCAR
Route::get('search', 'SearchController@search');


//
Route::middleware(['auth'])->prefix('admin')->namespace('Admin')->group(function(){


    Route::get('/products', 'ProductController@index')->name('list_products'); //listar
    Route::get('/products/create', 'ProductController@create')->name('create_product');
    Route::post('/products', 'ProductController@store')->name('product_store'); //almacenar producto
    Route::get('/products/edit/{product}', 'ProductController@edit'); //editar producto
    Route::post('/products/update/{product}', 'ProductController@update'); //guardar cambios producto
    Route::delete('/products/{product}', 'ProductController@delete'); //eliminar producto

    Route::get('/products/images/{id}', 'ImageController@index'); //listar imagenes
    Route::post('/products/images/{id}', 'ImageController@store'); //almacenar imagenes
    Route::delete('/products/images/{image}', 'ImageController@delete'); //eliminar imagen
    Route::get('/products/images/select/{id}/{image_id}', 'ImageController@destacar_imagen'); //destacar imagen

    Route::get('/categories', 'CategoryController@index')->name('list_categories'); //listar categorias
    Route::get('/categories/create', 'CategoryController@create')->name('create_category'); //formulario de registro de categorias
    Route::post('/categories', 'CategoryController@store')->name('category_store');; //almacenar categoria
    Route::get('/categories/edit/{category}', 'CategoryController@edit'); //editar categoria
    Route::post('/categories/update/{category}', 'CategoryController@update'); //guardar cambios categoria
    Route::delete('/categories/{category}', 'CategoryController@delete'); //eliminar categoria

});



